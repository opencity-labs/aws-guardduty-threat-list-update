# AWS GuardDuty Threat List Update

Download the latest OTX reputation list, upload to S3, and update GuardDuty to use it

## Usage

one-shot usage
```
docker run -e AWS_SECRET_ACCESS_KEY=... -e AWS_ACCESS_KEY_ID=... -it --rm <image>
```

docker-compose
```
services:
  guardduty-threats-update:
    image: <image>
    environment:
      AWS_SECRET_ACCESS_KEY: ...
      AWS_ACCESS_KEY_ID: ...
```


## Credits
 
Original script from [https://github.com/UranusBytes/icinga-plugins](Jeremy Phillips)

