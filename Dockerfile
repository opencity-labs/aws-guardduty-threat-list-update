FROM demisto/boto3py3:1.0.0.82871

WORKDIR /srv

ADD requirements.txt /srv

RUN pip install --no-cache-dir -r requirements.txt

ADD update.py /srv


CMD "./update.py"
